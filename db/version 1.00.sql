CREATE TABLE login(
id int primary key auto_increment,
name varchar (255) not null,
surname varchar (255) not null,
email varchar(255)not null,
password varchar(255) not null,
admin int default 0,
INDEX (id,email)
)ENGINE = InnODB;