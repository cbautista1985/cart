<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\Product;
use Libs\AppSession;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends BaseController
{
    protected $idsProduct = array();

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/cart/addItem", name ="addItemCart")
     */
    public function addCart(Request $request)
    {
        try {
            $products = new Product($this->krypto);
            $cart = new Cart();
            $idProducts = $request->get('ids');

            foreach ($idProducts as $idProduct) {
                /** @var  Product $productSelected */
                $productSelected = $products->findProductById(intval($idProduct), $this->session);
                $cart->addItem(
                    intval($idProduct),
                    $productSelected->getDescription(),
                    $productSelected->getType(),
                    $productSelected->getPrice()
                );
            }

            $this->session->set(AppSession::_SESSION_CART_, $cart->getCart());
            $this->session->set(AppSession::_SESSION_COUNT_CART, $cart->countItems());
            $this->session->set(
                AppSession::_SESSION_TOTAL_PRICE_CART,
                $cart->getTotalPriceCart($cart->cart)
            );

            return new JsonResponse(
                array(
                    "status" => Response::HTTP_OK,
                    'count'  => $cart->countItems(),
                )
            );
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }

    /**
     * @Route("/cart/show", name ="showCart")
     */
    public function showCart()
    {
        return $this->render(
            'Cart/index.html.twig',
            array(
                'section'  => $this->section,
                'cart'     => $this->session->get(AppSession::_SESSION_CART_),
                'totalPrice' => $this->session->get(AppSession::_SESSION_TOTAL_PRICE_CART)
            )
        );
    }
}
