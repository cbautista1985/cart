<?php
namespace App\Controller;

use App\Entity\Product;
use Libs\AppSession;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends BaseController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $product = new Product($this->krypto);
        $products = $product->getListProduct();

        $this->session->set(AppSession::_SESSION_ALL_PRODUCTS, $products);

        return $this->render(
            'Product/index.html.twig',
            array(
                'section'  => $this->section,
                'products' => $products
            )
        );
    }
}