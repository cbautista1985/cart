<?php
namespace App\Entity;

class Cart
{
    const MIN_ITEM = 2;
    const METHOD_ADD_CART = 'addCart';

    protected $session;
    public $cart = array();

    public function addItem($id, $description, $type, $price, $quantity = 1)
    {
        $this->cart[$id]['id'] = (isset($this->cart[$id]['id'])) ? $this->cart[$id]['id'] : $id;
        $this->cart[$id]['description'] = (isset($this->cart[$id]['description']))
            ? $this->cart[$id]['description'] : $description;
        $this->cart[$id]['type'] = (isset($this->cart[$id]['type'])) ? $this->cart[$id]['type'] : $type;
        $this->cart[$id]["qty"] = (isset($this->cart[$id]['qty'])) ? ($this->cart[$id]["qty"] + $quantity) : $quantity;
        $this->cart[$id]['price'] = $this->getPriceByTypeAorB($id, $price) ;
    }


    /**
     * @return float|int
     */
    public function countItems()
    {
        return array_sum(array_column($this->cart, 'qty'));
    }

    /**
     * @return array
     */
    public function getCart()
    {
        return $this->cart;
    }

    private function getPriceByTypeAorB($id, $price, $dto = 10)
    {

        $final = (isset($this->cart[$id]['price']))? ($this->cart[$id]['price'] + $price) : $price;
        if ($this->cart[$id]['type'] == Product::TYPE_A) {
            if ($this->cart[$id]['qty'] == 2) {
                $discount = (($this->cart[$id]['price'] + $price) * $dto )/100;
                $final = ($this->cart[$id]['price'] + $price) - $discount;
            }
        }
        if (isset($this->cart[$id]['price'])) {
            if ($this->cart[$id]['type'] == Product::TYPE_B) {
                $final = ($this->cart[$id]['price'] + $price) - 5;
            } else {
                $final = $price - 5;
            }
        }
        return $final;
    }

    public function getTotalPriceCart($cart, $dto = 5)
    {
        $total = 0.00;

        foreach ($cart as $key => $value) {
            $total = $total + $value['price'];
        }
        if ($total >= 40) {
            $disc = ($total * $dto)/100;
            $total = $total - $disc;
        }
        return number_format($total, 2);
    }
}
