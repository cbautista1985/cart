<?php
namespace App\Entity;

use Libs\AppSession;
use Libs\Krypto;

class Product
{
    private $products = array();

    private $id;
    private $name;
    private $description;
    private $price;
    private $active;
    private $type;
    private static $types = ['A', 'B', 'C'];
    const TYPE_A = 'A';
    const TYPE_B = 'B';
    private static $allPrice = [10, 8, 12];

    private $krypto;

    /**
     * Product constructor.
     * @param Krypto $krypto
     */
    public function __construct(Krypto $krypto)
    {
        $this->krypto = $krypto;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts(array $products): void
    {
        $this->products = $products;
    }

    /**
     * @return array
     */
    public function getListProduct()
    {
        for ($i = 1; $i <= 25; $i++) {
            $product = new Product($this->krypto);
            $product->setId($i);
            $product->setActive(1);
            $product->setName('Product ' .$i);
            $product->setDescription('Description of product');
            $product->setType(Product::$types[array_rand(Product::$types)]);
            $this-> setPriceByType($product);
            array_push($this->products, $product);
        }
        return $this->products;
    }


    /**
     * @param Product $product
     */
    private function setPriceByType(Product $product)
    {
        if ($product->getType() == 'A') {
            $product->setPrice(Product::$allPrice[0]);
        } elseif ($product->getType() == 'B') {
            $product->setPrice(Product::$allPrice[1]);
        } else {
            $product->setPrice(Product::$allPrice[2]);
        }
    }

    /**
     * @param $id
     * @param AppSession $appSession
     * @return false|mixed
     */
    function findProductById($id, AppSession $appSession)
    {
        $i = 0;
        $this->setProducts($appSession->get(AppSession::_SESSION_ALL_PRODUCTS));
        foreach ($this->getProducts() as $value) {
            if ($value->id == $id) {
                return $this->getProducts()[$i];
            }
            $i++;
        }
        return false;
    }

}