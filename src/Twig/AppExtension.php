<?php
namespace App\Twig;

use Libs\Krypto;
use Symfony\Bundle\TwigBundle\DependencyInjection\TwigExtension;
use Twig\TwigFilter;

class AppExtension extends TwigExtension
{
    public function getFilters()
    {
        return [
            // the logic of this filter is now implemented in a different class
            new TwigFilter('crypt', [Krypto::class, 'crypt']),
        ];
    }
}