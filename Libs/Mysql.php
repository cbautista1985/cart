<?php
namespace Libs;


class MySql
{
    private $host;
    private $port;
    private $database;
    private $user;
    private $password;
    private $db;

    private $inTransaction = false;
    private $stmt;
    private $query;
    /**
     * @var null
     */
    private $trans;

    /**
     * MySql constructor.
     * @param $host
     * @param $port
     * @param $database
     * @param $user
     * @param $password
     * @param null $trans
     * @throws SQLException
     */
    public function __construct($host, $port, $database, $user, $password, $trans = null)
    {
        $this->trans = $trans;
        try {
            $this->host = $host;
            $this->port = $port;
            $this->database = $database;
            $this->user = $user;
            $this->password = $password;

            $this->db = new \PDO(
                'mysql:host=' . $this->host . ';port=' . $this->port . ';dbname=' . $this->database,
                $this->user,
                $this->password
            );
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->setQuery('SET NAMES \'utf8\'');
            $this->execSQL();
            /** @var $this->transaction trans */
        } catch (\PDOException $e) {

        }
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function getDatabase()
    {
        return $this->database;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function changeDB($database)
    {
        try {
            $this->database = $database;

            $this->db = new \PDO(
                'mysql:host=' . $this->host . ';port=' . $this->port . ';dbname=' . $this->database,
                $this->user,
                $this->password
            );
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new SQLException('Could not connect to database ' . $this->database, $e->getMessage());
        }
    }

    public function __destruct()
    {
        $this->db = null;
    }

    public function beginTransaction()
    {
        if (!$this->inTransaction) {
            if (!$this->db->beginTransaction()) {
                throw new SQLException('Could not begin transaction');
            }
            $this->inTransaction = true;
        }
        return true;
    }

    public function commit()
    {
        if ($this->inTransaction) {
            $this->db->commit();
            $this->inTransaction = false;
        }
    }

    public function getLastInsertID()
    {
        return $this->db->lastInsertId();
    }


    public function rollBack()
    {
        if ($this->inTransaction) {
            $this->db->rollBack();
            $this->inTransactionnsaction = false;
        }
    }

    public function inTransaction()
    {
        return $this->inTransaction;
    }

    public function setQuery($query)
    {
        $this->query=$query;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function getFinalQuery($values = array())
    {
        if ($this->values == null) {
            $this->values = $values;
        }

        return $this->debugQuery();
    }

    public function getDB()
    {
        return $this->db;
    }

    public function closeCursor()
    {
        $this->stmt->closeCursor();
    }

    public function getNumRows($result = null)
    {
        if ($result!=null) {
            return $result->rowCount();
        } elseif ($this->stmt!=null) {
            return $this->stmt->rowCount();
        } else {
            return 0;
        }
    }

    /**
     * @param array $values
     * @return bool|\PDOStatement
     */
    public function execSQL($values = array())
    {
        try {
            $this->values = $values;
            $this->stmt = $this->db->prepare($this->getQuery());
            $this->stmt->execute($values);
            return $this->stmt;
        } catch (\PDOException $e) {
            throw new SQLException('Error in query execution', $e->getMessage(), $this->getFinalQuery($values));
        }
    }

    /**
     * @param null $result
     * @return int|mixed|null
     */
    public function getLastError($result = null)
    {
        if ($result!=null) {
            $error = $result->errorInfo();
        } elseif ($this->stmt!=null) {
            $error = $this->stmt->errorInfo();
        } else {
            return 0;
        }

        if ($error[1]) {
            return $error[2];
        }

        return null;
    }

    /**
     * @param null $result
     * @return false
     */
    public function fetchQuery($result = null)
    {
        if ($result!=null) {
            return $result->fetch();
        } elseif ($this->stmt!=null) {
            return $this->stmt->fetch();
        } else {
            return false;
        }
    }

    /**
     * @param null $result
     * @return false
     */
    public function fetchAllQuery($result = null)
    {
        if ($result!=null) {
            return $result->fetchAll();
        } elseif ($this->stmt!=null) {
            return $this->stmt->fetchAll();
        } else {
            return false;
        }
    }

    /**
     * @param null $result
     * @return false
     */
    public function fetchAllQueryAssoc($result = null)
    {
        if ($result!=null) {
            return $result->fetchAll(\PDO::FETCH_ASSOC);
        } elseif ($this->stmt!=null) {
            return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            return false;
        }
    }

    /**
     * @param $class
     * @param null $result
     * @return false
     */
    public function fetchAllQueryObject($class, $result = null)
    {
        if ($result!=null) {
            return $result->fetchAll(\PDO::FETCH_CLASS, $class);
        } elseif ($this->stmt!=null) {
            return $this->stmt->fetchAll(\PDO::FETCH_CLASS, $class);
        } else {
            return false;
        }
    }

    public function fetchQueryAssoc($result = null)
    {
        if ($result!=null) {
            return $result->fetch(\PDO::FETCH_ASSOC);
        } elseif ($this->stmt!=null) {
            return $this->stmt->fetch(\PDO::FETCH_ASSOC);
        } else {
            return false;
        }
    }

    public function fetchQueryObject($class, $result = null)
    {
        if ($result!=null) {
            return $result->fetchObject($class);
        } elseif ($this->stmt!=null) {
            return $this->stmt->fetchObject($class);
        } else {
            return false;
        }
    }

    public function fetchQueryArray($result = null)
    {
        return $this->fetchQuery($result);
    }


    public function fetchQueryRow($result = null)
    {
        return $this->fetchQuery($result);
    }


    private function debugQuery()
    {
        $query=$this->getQuery();
        $params=$this->values;
        $keys=array();

        if ($params==null) {
            return $query;
        }

        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[]='/:' . $key . '/';
            } else {
                $keys[]='/[?]/';
            }
            if (is_array($value)) {
                $params[$key]=implode(',', $value);
            }
            if (is_null($value)) {
                $params[$key]='NULL';
            }
            if (!is_numeric($value)) {
                $params[$key]="'" . $value . "'";
            }
        }

        return preg_replace($keys, $params, $query, 1, $count);
    }
}