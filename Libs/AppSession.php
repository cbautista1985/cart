<?php
namespace Libs;

use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Session\Session;

class AppSession extends Session
{
    const _SESSION_ALL_PRODUCTS = 'productsAll';
    const _SESSION_CART_ = 'cartAll';
    const _SESSION_COUNT_CART = 'CartCountItem';
    const _SESSION_TOTAL_PRICE_CART = 'CartTotalPrice';

    protected $config;
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();

        $this->container = $container;
    }

    public function renewSession()
    {
        new Cookie($this->getName(), $this->getId());
    }

    public function openSession($sessionInfo)
    {
        foreach ($sessionInfo as $index => $value) {
            $this->set($index, $value);
        }
    }

    public function closeSession()
    {
        $this->start();
        $this->invalidate();
    }
}